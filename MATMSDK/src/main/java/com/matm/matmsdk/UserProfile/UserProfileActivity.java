package com.matm.matmsdk.UserProfile;

import android.os.Bundle;
 import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.matm.matmsdk.Utils.EnvData;
import isumatm.androidsdk.equitas.R;

public class UserProfileActivity extends AppCompatActivity {

    TextView UserName;
    TextView YourName;
    EditText firstName;
    EditText lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        UserName = findViewById(R.id.userName);
        YourName = findViewById(R.id.yourName);

        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);

        UserName.setText(EnvData.UserName);
        YourName.setText(EnvData.userProfile.getFirstName() +" "+ EnvData.userProfile.getLastName() );
        firstName.setText(EnvData.userProfile.getFirstName());
        lastName.setText(EnvData.userProfile.getLastName());
    }
}
